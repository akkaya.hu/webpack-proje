WEbpack kullanmaya yeni başlayanlar için basit bir webpack proje yapısı.


Projede kullanılan modüller:

  "devDependencies": {
  
    "clean-webpack-plugin": "^0.1.19",
    
    "html-webpack-plugin": "^3.2.0",
    
    "webpack": "^4.19.1",
    
    "webpack-cli": "^3.1.0",
    
    "webpack-dev-server": "^3.1.8"
    
  },
